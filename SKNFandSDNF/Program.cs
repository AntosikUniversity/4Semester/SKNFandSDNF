﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SKNFandSDNF
{
    class Program
    {
        const string InFileName = "in.txt";
        const string OutTableName = "table.txt";
        const string OutSKNFName = "sknf.txt";
        const string OutSDNFName = "sdnf.txt";

        static void Main(string[] args)
        {
            string formulaStr = "A|B&C|D";
            if (File.Exists(InFileName))
                formulaStr = File.ReadAllText(InFileName);
            else
                throw new Exception("no file 'in.txt' found!");

            List<string> formulas = formulaStr.Split('\n').ToList<string>();
            int[] consts = { 0, 1 };

            foreach (string formula in formulas)
            {

                List<char> variables = new List<char>();            // unique

                // GET VARIABLES LIST
                variables = formula
                    .Where(c => Char.IsLetter(c))
                    .ToList<char>()/*
                    .Select(c => Char.ToUpper(c))                   // if A == a
                    .ToList<char>()*/
                    .Distinct()
                    .ToList<char>();

                int variantsCount = 2 << variables.Count - 1;

                bool[,] variants = new bool[variables.Count + 1, variantsCount];
                for (int i = 0, lenRow = variantsCount; i < lenRow; i++)
                {
                    // GENERATE TABLE
                    for (int j = 0, len = variables.Count; j < len; j++)
                    {
                        if (j == 0)
                        {
                            if (i < lenRow / 2)
                                variants[j, i] = false;
                            else
                                variants[j, i] = true;
                        }
                        else if (j == len - 1)
                        {
                            if (i % 2 == 0)
                                variants[j, i] = false;
                            else
                                variants[j, i] = true;
                        }
                        else
                        {
                            if (i % (2 << variables.Count - (j + 1)) < (2 << (variables.Count - (j + 2))))
                                variants[j, i] = false;
                            else
                                variants[j, i] = true;
                        }
                    }

                    // BUILD NEW FORMULA WITH CONSTANTS AND CALCULATE IT
                    string newFormula = formula;
                    for (int j = 0, len = variables.Count; j < len; j++)
                        newFormula = newFormula.Replace(variables[j], variants[j, i] ? '1' : '0');
                    variants[variables.Count, i] = calculate(newFormula + ';');
                }

                // PRINT TABLE
                Console.WriteLine("\n\n TABLE\n{0}", printTable(variants, variables));
                File.WriteAllText(OutTableName, printTable(variants, variables));

                // PRINT SDNF
                Console.WriteLine("\n\n SDNF\n{0}", SDNF(variants, variables));
                File.WriteAllText(OutSDNFName, printTable(variants, variables));

                // PRINT SKNF
                Console.WriteLine("\n\n SDNF\n{0}", SDNF(variants, variables));
                File.WriteAllText(OutSKNFName, printTable(variants, variables));

                return;
            }
        }

        static bool calculate(string formula)
        {
            char _c = ' ';
            char nowChar = ' ';
            Stack<char> operators = new Stack<char>();
            operators.Push('(');
            Stack<bool> vars = new Stack<bool>();
            bool Prev = false, Now = false, Result = false;
            Console.WriteLine("\nLOGS for {0}:", formula);

            foreach (char c in formula)
            {

                if (c == '&' || c == '|' || c == '(' || c == ')' || c == ';')
                {
                    if (Char.IsDigit(_c))
                    {
                        vars.Push(_c == '1' ? true : false);
                    }
                    if (c == '(')
                    {
                        operators.Push(c);
                    }
                    else if (c == '&' || c == '|' || c == '%')
                    {
                        if (priority(c) <= priority(operators.Peek()))
                        {
                            Now = vars.Pop(); Prev = vars.Pop();
                            switch (nowChar = operators.Pop())
                            {
                                case '&': Result = Prev & Now; Console.WriteLine("{0} & {1} = {2}", Prev, Now, Result); break;
                                case '|': Result = Prev | Now; Console.WriteLine("{0} | {1} = {2}", Prev, Now, Result); break;
                                default: Console.WriteLine("Undefined!"); break;
                            }
                            vars.Push(Result);
                        }
                        operators.Push(c);
                    }
                    else if (c == ')' || c == ';')
                    {
                        do
                        {
                            nowChar = operators.Pop();
                            if (nowChar == '(' || nowChar == ' ') break;
                            Now = vars.Pop(); Prev = vars.Pop();
                            switch (nowChar)
                            {
                                case '&': Result = Prev & Now; Console.WriteLine("{0} & {1} = {2}", Prev, Now, Result); break;
                                case '|': Result = Prev | Now; Console.WriteLine("{0} | {1} = {2}", Prev, Now, Result); break;
                                default: Console.WriteLine("Undefined!"); break;
                            }
                            vars.Push(Result);
                        } while (nowChar != '(' && nowChar != ' ');
                        return vars.Peek();
                        if (nowChar == '(' && c == ';')
                        {
                            char cc = '(';
                            operators.Clear(); vars.Clear();
                            operators.Push(cc);
                            vars.Push(false);
                            Prev = false; Now = false; Result = false;
                            Console.WriteLine("\n\n-----------------------------------\n");
                        }
                    }
                }
                _c = c;
            }
            return vars.Peek();
        }

        static int priority(char oper)
        {
            switch (oper)
            {
                case '(': case ')': return 0;
                case '&': return 2;
                case '|': return 1;
                default:
                    break;
            }
            return 2;
        }

        static int boolean(bool b)
        {
            return (b) ? 1 : 0;
        }

        static string SDNF(bool[,] table, List<char> vars)
        {
            int variantsCount = 2 << vars.Count - 1;
            var sb = new StringBuilder();
            for (int i = 0, lenRow = variantsCount; i < lenRow; i++)
            {
                if (table[vars.Count, i] == true)
                {
                    sb.Append('(');
                    for (int j = 0, len = vars.Count; j < len; j++)
                    {
                        if (table[j, i] == false) sb.Append('-');
                        sb.Append(String.Format("{0}", vars[j]));
                        if (j != len - 1) sb.Append('&');
                    }
                    sb.Append(")");
                    if (i != lenRow - 1) sb.Append("|");
                }
            }

            return sb.ToString();
        }

        static string SKNF(bool[,] table, List<char> vars)
        {
            int variantsCount = 2 << vars.Count - 1;
            var sb = new StringBuilder();
            for (int i = 0, lenRow = variantsCount; i < lenRow; i++)
            {
                if (table[vars.Count, i] == false)
                {
                    sb.Append('(');
                    for (int j = 0, len = vars.Count; j < len; j++)
                    {
                        if (table[j, i] == true) sb.Append('-');
                        sb.Append(String.Format("{0}", vars[j]));
                        if (j != len - 1) sb.Append('|');
                    }
                    sb.Append(")");
                    if (i != lenRow - 1) sb.Append("&");
                }
            }

            return sb.ToString();
        }

        static string printTable(bool[,] table, List<char>vars)
        {
            int variantsCount = 2 << vars.Count - 1;

            var sb = new StringBuilder();
            foreach (char var in vars)
                sb.Append(String.Format("{0} ", var));
            sb.Append("RESULT ");
            sb.AppendLine();
            for (int i = 0, lenRow = variantsCount; i < lenRow; i++)
            {
                for (int j = 0, len = vars.Count + 1; j < len; j++)
                {
                    sb.Append(String.Format("{0} ", boolean(table[j, i])));
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }
    }
}
